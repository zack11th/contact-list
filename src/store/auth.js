import axios from 'axios'
import {url} from './api.config'


export default({
  namespaced: true,
  state: {
    authentication: localStorage.getItem('user-auth') || false,
    user: localStorage.getItem('user-name') || '',
    uid: localStorage.getItem('user-id') || null,
    loading: false,
    error: ''
  },
  mutations: {
    setLoading(state, value) {
      state.loading = value;
    },
    setError(state, message) {
      state.error = message;
    },
    authSuccess(state, user) {
      localStorage.setItem('user-auth', true);
      localStorage.setItem('user-name', user.name);
      localStorage.setItem('user-id', user.id);
      state.authentication = true;
      state.user = user.name;
      state.uid = user.id;
    },
    logout(state) {
      localStorage.removeItem('user-auth');
      localStorage.removeItem('user-name');
      localStorage.removeItem('user-id');
      state.authentication = false;
      state.user = '';
      state.uid = '';
    }
  },
  actions: {
    login({commit}, {login, password}) {
      commit('setLoading', true);
      commit('setError', '');
      return new Promise((resolve, reject) => {
        axios.get(`${url}/users?name=${login}&password=${password}`)
          .then(response => {
            commit('setLoading', false);
            if(response.data.length > 0) {
              commit('authSuccess', {
                name: response.data[0].name,
                id: response.data[0].id
              });
              resolve();
            } else {
              commit('setError', 'We cannot find an account with that login or password');
              reject();
            }
          }, error => {
            commit('setError', 'Something went wrong');
            commit('setLoading', false);
            reject();
          });
      });
    },
    logout({commit}) {
      commit('logout');
    }
  },
  getters: {
    authentication(state) {
      return state.authentication;
    },
    user(state) {
      return state.user;
    },
    uid(state) {
      return state.uid;
    },
    loading: (state) => {
      return state.loading;
    },
    error: (state) => {
      return state.error;
    }
  }
})