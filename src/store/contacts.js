import axios from 'axios'
import {url} from './api.config'

export default({
  namespaced: true,
  state: {
    contacts: [],
    loading: false,
    message: ''
  },
  mutations: {
    setContacts(state, data) {
      state.contacts = data;
    },
    addContact(state, contact) {
      state.contacts.push(contact);
    },
    editContact(state, contact) {
      state.contacts = state.contacts.map(obj => {
        if(obj.id === contact.id) {
          return contact;
        }
        return obj;
      });
    },
    removeContact(state, id) {
      state.contacts = state.contacts.filter(el => el.id !== id);
    },
    clearState(state) {
      state.contacts = [];
    },
    setLoading(state, value) {
      state.loading = value;
    },
    setMessage(state, message) {
      state.message = message;
    }
  },
  actions: {
    getContacts({commit}, id) {
      commit('setMessage', '');
      commit('setLoading', true);
      axios.get(`${url}/${id}`)
        .then(response => {
          commit('setLoading', false);
          commit('setContacts', response.data);
        }, error => {
          commit('setLoading', false);
          commit('setMessage', 'Something went wrong');
        });
    },
    removeContact({commit}, {uid, id}) {
      commit('setMessage', '');
      axios.delete(`${url}/${uid}/${id}`)
        .then(response => {
          commit('removeContact', id);
          commit('setMessage', 'Contact has been deleted');
        }, error => {
          commit('setMessage', 'Something went wrong');
        });
    },
    editContact({commit}, {uid, contact}) {
      commit('setMessage', '');
      axios.patch(`${url}/${uid}/${contact.id}`, contact)
        .then(response => {
          commit('editContact', response.data);
          commit('setMessage', 'Contact has been changed');
        }, error => {
          commit('setMessage', 'Something went wrong');
        });
    },
    addContact({commit}, {uid, contact}) {
      commit('setMessage', '');
      axios.post(`${url}/${uid}`, contact)
        .then(response => {
          commit('addContact', response.data);
          commit('setMessage', 'Contact has been added');
        }, error => {
          commit('setMessage', 'Something went wrong');
        });
    },
    clearState({commit}) {
      commit('clearState');
    }
  },
  getters: {
    contacts(state) {
      return state.contacts;
    },
    loading(state) {
      return state.loading;
    },
    message(state) {
      return state.message;
    }
  }
})