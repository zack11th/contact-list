import Vue from 'vue'
import Vuex from 'vuex'
import contacts from './contacts'
import auth from './auth'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    contacts, auth
  },
  strict: process.env.NODE_ENV !== 'production'
})
