import Vue from 'vue'
import VueRouter from 'vue-router'
import Contacts from '@/views/Contacts.vue'
import Auth from '@/views/Auth.vue'
import store from '@/store'

Vue.use(VueRouter);

const ifAuthenticated = (to, from, next) => {
  if (store.getters['auth/authentication']) {
    next();
    return;
  }
  next({name: 'auth'});
};
const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters['auth/authentication']) {
    next();
    return;
  }
  next({name: 'contacts'});
};

const routes = [
  {
    path: '/',
    name: 'contacts',
    component: Contacts,
    beforeEnter: ifAuthenticated
  },
  {
    path: '/login',
    name: 'auth',
    component: Auth,
    beforeEnter: ifNotAuthenticated
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router
