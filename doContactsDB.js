const axios = require('axios');

const firstNames = ["Emilу", "Anna", "Olivia", "Allison", "Trinity", "James", "Bryan", "John", "Kevin", "Richard"];
const lastNames = ["Adams", "Anderson", "Campbell", "Davis", "Edwards", "Evans", "Harris", "Lee", "King", "Miller"];

function randomInteger(min, max) {
  let rand = min + Math.random() * (max + 1 - min);
  return Math.floor(rand);
}

function createName() {
  return (`${firstNames[randomInteger(0, 9)]} ${lastNames[randomInteger(0, 9)]}`);
}

function createNumber() {
  let num = '+';
  for(let i = 0; i < 11; i++) {
    num += randomInteger(0, 9)
  }
  return num;
}

function createContactList() {
  let list = [];
  for(let i = 0; i < 20; i++) {
    let name = createName();
    let phone = createNumber();
    list.push({name, phone, id: i});
  }
  return list;
}

axios.post('http://localhost:3000/1', createContactList());
axios.post('http://localhost:3000/2', createContactList());